<?php
if(isset($_POST["add_to_cart"]))
{
    if(isset($_SESSION["po_detail"]))
    {
        $item_array_idprd = array_column($_SESSION["po_detail"], "item_idprd");
        // if(!in_array($_GET["idprd"], $item_array_idprd))
        // {
        $count = count($_SESSION["po_detail"]);
        $item_array = array(
            'item_idprd'            =>  $_GET["idprd"],
            'item_product'          =>  $_POST["product"],
            'item_principle'        =>  $_POST["nm_principle"],
            'item_qty'              =>  $_POST["qty"],
            'item_pack'             =>  $_POST["pack"],
            'item_final'            =>  $_POST["final"],
            'item_arrive'           =>  $_POST["arrive"],
            'item_shipment'         =>  $_POST["ship"],
            'item_lead'             =>  $_POST["lead"],
            'item_selling_price'    =>  $_POST["selling_price"]

        );
        $_SESSION["po_detail"][$count] = $item_array;
        // }
        // else
        // {
        //  echo '<script>alert("Item Already Added")</script>';
        // }
    }
    else
    {
        $item_array = array(
            'item_idprd'            =>  $_GET["idprd"],
            'item_product'          =>  $_POST["product"],
            'item_principle'        =>  $_POST["nm_principle"],
            'item_qty'              =>  $_POST["qty"],
            'item_pack'             =>  $_POST["pack"],
            'item_final'            =>  $_POST["final"],
            'item_arrive'           =>  $_POST["arrive"],
            'item_shipment'         =>  $_POST["ship"],
            'item_lead'             =>  $_POST["lead"],
            'item_selling_price'    =>  $_POST["selling_price"]
        );
        $_SESSION["po_detail"][0] = $item_array;
    }
}

if(isset($_GET["action"]))
{
    if($_GET["action"] == "delete")
    {
        foreach($_SESSION["po_detail"] as $keys => $values)
        {
            if($values["item_idprd"] == $_GET["idprd"])
            {
                unset($_SESSION["po_detail"][$keys]);
                echo '<script>alert("Item Removed")</script>';
                echo '<script>window.location="cosmetic.php?page=inputpo"</script>';
            }
        }
    }
}

?>


<div class="container-fluid">
    <h1 class="mt-4">Input PO </h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">Purchase Order</li>
        <li class="breadcrumb-item active">Input PO</li>
    </ol>



    <div class="row">
        <div class="col-md-12">
            <form class="form" action="inputpo_insert.php" method="POST">
                <table class="table ">
                    <tr>
                        <td style="vertical-align: middle;width: 15%">Tanggal Transaksi</td>
                        <td >
                            <input type="text" class="form-control " id="tgl_transaksi" name="tgl_transaksi" value="<?= date('d F Y');?>" readonly>
                        </td>
                        <td style="vertical-align: middle; width: 7%; text-align: right">Sales</td>
                        <td >
                            <input type="hidden" class="form-control " id="idsales" name="idsales" value="<?= $iduser;?>" >
                            <input type="text" class="form-control " id="sales" name="sales" value="<?= $username;?>" readonly>
                        </td>
                    </tr>

                    <tr>
                        <td>NO PO</td>
                        <td><input type="text" class="form-control" id="nopo" name="nopo" placeholder="KI/PP/CS/001"></td>
                    </tr>
                    <tr>
                        <td colspan="12"><button type="submit" class="btn btn-primary">Submit</button></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h1>Detail PO</h1>
            <?php
            $result  = mysqli_query($conn,"SELECT * FROM dt_product
                                                INNER JOIN dt_principle
                                                ON dt_product.idprc=dt_principle.idprc");

            while ($row = mysqli_fetch_array($result))
            {
            $idprd=$row["idprd"];

            ?>

            <form method="post" action="cosmetic.php?page=inputpo&action=add&idprd=<?php echo $idprd; ?>">
                <!-- <form method="post" action="cosmetic.php?page=inputpo&action=add">           -->

                <?php } ?>

                <table class="table table-responsive " >
                    <tr>
                        <th>Product</th>
                        <th>Principle</th>
                        <th>Pack</th>
                        <th>Qty</th>
                        <th>Final PO (Kg)</th>
                        <th>Arrive</th>
                        <th>Ship Mode</th>
                        <th>Lead Time</th>
                        <th>Selling Price</th>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle;">

                            <select name="idprd" id="idprd" onchange="changeValue(this.value)">
                                <option value="0">---Pilih---</option>
                                <?php
                                $result = mysqli_query($conn,"SELECT * FROM dt_product INNER JOIN dt_principle ON dt_product.idprc=dt_principle.idprc");
                                $jsArray = "var  data = new Array();\n";
                                while ($row = mysqli_fetch_array($result))
                                {
                                    echo '<option value="'.$row['idprd'].'">' .$row['product'] . '</option>';
                                    $jsArray .= "data['" .$row['idprd'] ."']=
                                {
                                    idprc:'" .addslashes($row['idprc']) ."',
                                    product:'" .addslashes($row['product']) ."',
                                    principle:'" .addslashes($row['principle']) ."',
                                    pack:'" .addslashes($row['pack']) ."'
                                };\n";
                                }

                                //addslashes : menambahkan simbol backslash (\) yang berfungsi untuk mencegah hacker memasukkan script yang mengandung kutip pada website.
                                ?>
                            </select>
                            <input type="hidden" name="idprd" id="idprd"/>
                            <input type="hidden" name="product" id="product"/>
                        </td>
                        <!--<td style="vertical-align: middle;"></td>-->
                        <td>
                            <input type="hidden" name="id_principle" id="id_principle"/>
                            <input type="text" name="nm_principle" id="nm_principle">
                        </td>
                        <td><input type="text" name="pack" id="pack" size="5" onkeyup="sum();"></td>
                        <td><input type="text" name="qty" id="qty" size="5" onkeyup="sum();" ></td>
                        <td><b><input type="text" name="final" id="final" size="5" style="background-color: aqua; text-align: center"></b></td>
                        <td><input type="date" name="arrive"></td>

                        <td>

                            <select name="ship" id="ship">
                                <option disabled selected> --Ship Mode-- </option>
                                <?php
                                $sql=mysqli_query($conn, "SELECT * FROM ship");
                                while ($data=mysqli_fetch_array($sql)) {
                                    ?>
                                    <option value="<?=$data['shipment']?>"><?=$data['shipment']?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </td>

                        <td><input type="date" name="lead"></td>
                        <td><input type="text" name="selling_price" placeholder="Di input Sales" size="10"></td>
                        <td><button type="submit" id="btn-tambah-item" name="add_to_cart" class="btn btn-success">ADD</button><br></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <tr>
                    <th>No</th>
                    <th>Product</th>
                    <th>Principle</th>
                    <th>Pack</th>
                    <th>Qty</th>
                    <th>Final PO (Kg)</th>
                    <th>Arrive</th>
                    <th>Ship Mode</th>
                    <th>Lead Time</th>
                    <th>Selling Price</th>
                    <th>Aksi</th>
                </tr>
                <?php
                if(!empty($_SESSION["po_detail"]))
                {
                    $final = 1;
                    foreach($_SESSION["po_detail"] as $keys => $values)
                    {
                        ?>
                 <tr>
                            <?php
                            $i=1;
                            foreach ($data as $row) {
                                echo '<tr>
                    <td>'.$i.'</td>
                    <td>'.$row['item_idprd'].'</td>
                    <td>'.$row['item_product'].'</td>
                    <td>'.$row['item_principle'].'</td>
                    <td>'.$row['item_pack'].'</td>
                    <td>'.$row['item_qty'].'</td>
                    <td>'.$row[''].'</td>
                    <td>'.$row['item_arrive'].'</td>
                    <td>'.$row['item_shipment'].'</td>
                    <td>'.$row['item_shipment'].'</td>
                    <td>'.$row['item_shipment'].'</td>

                </tr>';
                                $i++;
                            }
                            ?>
                            <!-- <td><?php echo $values["item_idprd"]; ?></td>-->
                            <td><?php echo $values["item_product"]; ?></td>
                            <td><?php echo $values["item_principle"]; ?></td>
                            <td><?php echo $values["item_pack"]; ?></td>
                            <td><?php echo $values["item_qty"]; ?></td>
                            <td><?php echo number_format($values["item_qty"] * $values["item_pack"]);?></td>
                            <td><?php echo $values["item_arrive"]; ?></td>
                            <td><?php echo $values["item_shipment"]; ?></td>
                            <td><?php echo $values["item_lead"]; ?></td>
                            <!-- <td><?php echo $values["item_selling_price"]; ?></td>-->
                            <td><?php echo number_format($values["item_selling_price"]);?></td>
                            <td><a href="cosmetic.php?page=inputpo&action=delete&idprd=<?php echo $values["item_idprd"]; ?>"><span class="text-danger">Remove</span></a></td>
                        </tr>
                    <?php

                    }
                }
                ?>

            </table>

        </div>
    </div>
</div>

<!-- script autofield product -->
<script type="text/javascript">
    <?php echo $jsArray;?>
    function changeValue(idprd){
        document.getElementById('product').value = data[idprd].product; //data ini akan dikirim ke product
        document.getElementById('id_principle').value = data[idprd].idprc;
        document.getElementById('nm_principle').value = data[idprd].principle;
        document.getElementById('pack').value = data[idprd].pack;
        document.getElementById('qty').focus();
    };
</script>


<!-- script penjumlahan -->
<script>
    function sum() {
        var p = document.getElementById('pack').value;
        var q = document.getElementById('qty').value;
        var result = parseInt(p) * parseInt(q);
        if (!isNaN(result)) {
            document.getElementById('final').value = result;
        }
    }
</script>
