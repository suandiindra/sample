<?php
    $idpo = $_GET['id'];
    $idpo_ = $_GET['id'];
    $stats = "";
    $query_detail = "select po_detail_id,c.isconfirm as status,idproduct,b.product,final_harga,a.idprinciple,sales,a.pack,harga,qty,final,arrive,lead,idship 
    ,harga*final as amt  from inputpodetail a
    inner join dt_product b on a.idproduct = b.id 
                inner join inputpo c on c.idpo = a.idpo where a.idpo = '$idpo'";
    $res = mysqli_query($conn,$query_detail);
?>
<script>
    function totalHarga2(x){
        var a = document.getElementById("pack"+x).value;
        var b = document.getElementById("qty"+x).value;
        var c = document.getElementById("harga"+x).value;
        // var pack = document.getElementById("pack"+x).value;

        var hasil = parseInt(a)*parseInt(b);
        // var tot = hasil * c;
        console.log(hasil);
        if(isNaN(hasil) == true){
            hasil = 0;
        }
        document.getElementById("final"+x).innerHTML = hasil  ;
        // document.getElementById("amount"+x).innerHTML = tot  ;
    }

    function totalHarga3(x){
        var c = document.getElementById("harga"+x).value;
        var d = document.getElementById("final"+x).innerHTML;
       
        var hasil = parseInt(c)*parseInt(d);
        // var tot = hasil * c;
        console.log(hasil);
        if(isNaN(hasil) == true){
            hasil = 0;
        }
        document.getElementById("amount"+x).innerHTML = hasil  ;
    }

</script>
<div class="container-fluid">
	<h1 class="mt-4">Data PO </h1>
    <ol class="breadcrumb mb-4">
    	<li class="breadcrumb-item ">Purchase Order</li>
        <li class="breadcrumb-item active">Data PO Detail</li>
    </ol>
    <div class="row">
        <div class="col-md-12">
        <?php
        $roharga = "readonly";
        $ropack  = "readonly";
        if($_SESSION['level_id'] == '6'){
            $roharga = "";
        }
        
            $resx = mysqli_query($conn,$query_detail);
            $i = 0;

            while($dt = mysqli_fetch_array($resx)){
            $stats = $dt['status'];
            $idpack = "pack".$i;
            $idqty = "qty".$i;
            $idfinal = "final".$i;
            $total = "total".$i;
            $idamt = "amount".$i;
            $idharga = "harga".$i;
        ?>
            <form action="./actionpo.php" method="POST">
            <div class="card">
            <div class="card-header">
               <b><?php echo $dt['product'] ?> </b>
            </div>
            <div class="card-body">
                <input type="hidden" id="id" name = "id_unik" readonly value="<?php echo $dt['po_detail_id'] ?>"/>
                <div class="form-row">
                    <div class="col col-md-2">
                        <label for="">Principle</label>
                    </div>
                    :
                    <div class="col">
                    <b><label for=""><?php echo $dt['idprinciple'] ?></label></b>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col col-md-2">
                        <label for="">Sales</label>
                    </div>
                    :
                    <div class="col">
                    <b><label for=""><?php echo $dt['sales'] ?></label></b>
                    </div>
                </div>
                <br>
                <br>

                <!-- untuk detail -->
                <center>
                <div class="form-row">
                    <div class="col col-md-1" >
                        <b><label for="">Pack</label></b>
                        <input type="text" name="pack" id="<?php echo $idpack; ?>" <?php echo $ropack; ?> class="form-control" value="<?php echo $dt['pack']; ?>"/>
                    </div>
                    <div class="col col-md-2" >
                        <b><label for="">Harga</label></b>
                        <input type="text" name="harga" onkeyup="totalHarga3(<?php  echo $i?>)" id="<?php echo $idharga; ?>" class="form-control" <?php echo $roharga; ?> value="<?php echo $dt['harga']; ?>">
                    </div>
                    <div class="col col-md-1" >
                        <b><label for="">QTY</label></b>
                        <input type="text" name="qty" id="<?php echo $idqty; ?>" onkeyup="totalHarga2(<?php  echo $i?>)" class="form-control" value="<?php echo $dt['qty']; ?>">
                    </div>
                    <div class="col col-md-1" >
                        <b><label for="">Final Qty</label></b><br>
                        <span id = "<?php echo $idfinal; ?>" name="final" ><?php echo $dt['final']; ?></span>
                    </div>
                    <div class="col col-md-1" >
                        <b><label for="">Total Amount</label></b><br>
                        <span id = "<?php echo $idamt; ?>" name="amt" ><?php echo $dt['amt'] ?></span>
                    </div>
                    <div class="col col-md-1" >
                        <b><label for="">Arrive</label></b><br>
                        <span><?php echo $dt['arrive'] ?></span>
                    </div>
                    <div class="col col-md-2" >
                        <b><label for="">Shipment</label></b>
                        <select name="ship" class="form-control">
                        <option name="<?php echo $dt['idship'] ?>"><?php echo $dt['idship'] ?></option>
                        <?php
                            $selship = "select * from ship where shipment <> '".$dt['idship']."'";
                            $res = mysqli_query($conn,$selship);
                            while($dtship = mysqli_fetch_array($res)){
                        ?>  
                            <option name="<?php echo $dtship['shipment'] ?>"><?php echo $dtship['shipment'] ?></option>
                        <?php
                            }   
                        ?>
                        </select>
                    </div>
                    <div class="col col-md-1" style="padding-left:30px">
                        <b><label for="">Lead Time</label></b><br>
                        <span><?php echo $dt['lead'] ?></span>
                    </div>

                </div>
                </center>
            </div>
            <?php
            // echo $stats;
                if(($stats == 2) || ($stats != 2 && $_SESSION['level_id'] == '5') || ($_SESSION['level_id'] == '2') ){
//                if(($stats == 2) && $_SESSION['level_id'] == '2'){

                }else{
            ?>

            <div class="card-footer">
                <button name="delete" class="btn btn-danger">Delete</button>    
                <button name="edit" class="btn btn-warning">Edit</button>
            </div>
            <?php
                }
            ?>
            </form>
            </div>
            <br>
        <?php
            $i = $i +1;
            }
        ?>


             
        </div>
    </div>
    <?php
        if($_SESSION['level_id'] == '6' && $stats == 1 ){
    ?>
        <form action="./actionpo.php" method="POST">
            <div class="col">
                <input type="hidden" name="id" value="<?php echo $idpo; ?>">
                <input type="hidden" name="stat" value="2">
            </div>
            <br>
            <button class="btn btn-success" style="margin-left:12px" name="konfirmasi">Approve</button>
            <button class="btn btn-danger" style="margin-left:12px" name="reject">Reject</button>
        </form>
    <?php
        }elseif($_SESSION['level_id'] == '5' || $_SESSION['level_id'] == '1'){
    ?>
        <form action="./actionpo.php" method="POST">
            <div class="col">
                <input type="hidden" name="id" value="<?php echo $idpo; ?>">
                <label for="">Currency</label>
                <select name="curr" id="" class="form-control col-md-2">
                    <option value="">Pilih Mata Uang</option>
                    <?php
                        $res = mysqli_query($conn,"select * from m_currency");
                        while($dt = mysqli_fetch_array($res)){
                    ?>
                    <option value="<?php echo $dt['currency_id'] ?>"><?php echo $dt['description'] ?></option>
                    <?php
                        }
                    ?>
                </select>
            </div>
            <br>
            <button class="btn btn-success" style="margin-left:12px" name="konfirmasi">Konfirmasi</button>
        </form>
    <?php
        }
    ?>
    
</div>
