<?php
/**
 * Created by PhpStorm.
 * User: dias
 * Date: 18/03/2020
 * Time: 21:22
 */

 // untuk multiple condition based on level id
 $where = "where 1=1 ";
 $uname = $_SESSION['username'];
 if($_SESSION['level_id'] == '6'){
    $where = $where." and isconfirm not in (0) ";
 }elseif($_SESSION['level_id'] == '7'){
    $where = $where." and isconfirm in (2,3,4) ";
 }elseif($_SESSION['level_id'] == '1'){

 }
 elseif($_SESSION['level_id'] == '2'){
    $where = $where." and idsls = '$uname' ";
 }

 //untuk login purchasing proses
 if(isset($_GET['nopo'])){
    $nopo = $_GET['nopo'];
    $stat = $_GET['stat'];

    $proses = "update inputpo set isconfirm = $stat where po = '$nopo'";
    $res = mysqli_query($conn,$proses);
    // echo $proses;
    if($_SESSION['level_id'] == '7'){
        $cari_queri = "select * from inputpo a
        inner join dt_user b on a.idsls = b.username
        where po = '$nopo'";
        $res = mysqli_query($conn,$cari_queri);
        while($data = mysqli_fetch_array($res)){
            // $tujuan = $data['email'];
            $tujuan = "dias@id.chemicogroup.com";
            $subjek = "WAITING PO ";
            $isi    = "nomor po ini butuh di konfirmasi";
            kirimEmail($tujuan,$subjek,$isi);
        }

        $cari_queri = "";
        $res = mysqli_query($conn,$cari_queri);
        while($data = mysqli_fetch_array($res)){
            // $tujuan = $data['email'];
            $tujuan = "dias@id.chemicogroup.com";
            $subjek = "WAITING PO ";
            $isi    = "nomor po ini butuh di konfirmasi";
            kirimEmail($tujuan,$subjek,$isi);
        }

    }
 }


 $query = "select a.idpo,po,tgl,idsls, 
            case when isconfirm = 0 then 'Need Confirm' when isconfirm = 1 then 'Confirm' when isconfirm = 2 then 'Approved'
            when isconfirm = 3 then 'Proses'  when isconfirm = 4 then 'Selesai' else 'Rejected' end as status,
            description,
            sum(harga*final) as total,
            case when isconfirm = 0 then 'white' when isconfirm = 1 then 'Confirm' when isconfirm = 2 then 'green; color:white'
            when isconfirm = 3 then 'yellow; color:black'  when isconfirm = 4 then 'gray; color:black' else 'red' end as warna
            from inputpo a 
            inner join inputpodetail b on a.idpo = b.idpo
            left join m_currency c on c.currency_id = a.currency_id  $where
            group by a.idpo,po,tgl,idsls,isconfirm,description  ";
 echo $query;
 $respone = mysqli_query($conn,$query);
 ?>

<div class="container-fluid">
	<h1 class="mt-4">Data PO </h1>
    <ol class="breadcrumb mb-4">
    	<li class="breadcrumb-item ">Purchase Order</li>
        <li class="breadcrumb-item active">Data PO</li>
    </ol>
    <div class="row">
        <div class="col-md-12">
             <table class="table table-hover">
                <thead>
                <tr>
                    <td>Nomor</td>
                    <td>Nomor PO</td>
                    <td>Periode PO</td>
                    <td>Sales</td>
                    <td>Status PO</td>
                    <td>Currency</td>
                    <td>Total</td>
<!--                    <td>Principle</td>-->
                    <td>Action</td>
                    
                </tr>
                </thead>
                <?php
                $i = 1;
                while($data = mysqli_fetch_array($respone)){
                ?>
                <tbody >
                    <tr style="background-color:<?php echo $data['warna'] ?>">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $data['po'] ?></td>
                    <td><?php echo $data['tgl'] ?></td>
                    <td><?php echo $data['idsls'] ?></td>
                    <td><?php echo $data['status'] ?></td>
                    <td><?php echo $data['description'] ?></td>
<!--                    <td>--><?php //echo $data['total'] ?><!--</td>-->
                    <td><?php echo $data['total'] ?></td>
<!--                    <td>--><?php //echo $data['idprinciple'] ?><!--</td>-->
                    <td>
                        <a href="./cosmetic.php?page=datapodetail&id=<?php echo $data['idpo'] ?>">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                            Detail
                        </button>
                        </a>
                        <?php
                            if($_SESSION['level_id'] == '7'){
                        ?>
                            <a href="./cosmetic.php?page=datapo&nopo=<?php echo $data['po'] ?>&stat=3">
                            <button type="button" class="btn btn-danger">Proses</button>
                            </a>
                            <a href="./cosmetic.php?page=datapo&nopo=<?php echo $data['po'] ?>&stat=4">
                            <button type="button" class="btn btn-warning">Selesai</button>
                            </a>
                        <?php
                            }
                        ?>
                        
                    </td>
                    <tr>
                </tbody>
                <?php
                $i = $i +1;
                }
                ?>
            </table>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
                </div> -->
            </div>
            </div>
            <!-- modal -->

        </div>
    </div>
</div>
<script>
    jQuery(window).load(function(){
        jQuery('#exampleModal').modal('show').on('hide.bs.modal', function(e){
        e.preventDefault();
        });
    });
</script>