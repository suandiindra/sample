<link href="../css/styles.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>


<table class="table table-striped">
    <thead>

    <tr>
    <center>
        <th scope="col">No</th>
        <th scope="col">Nama Barang</th>
        <th scope="col">Principle</th>
        <th scope="col"><center>Pack Size (kg)</center></th>

    </tr>

    </thead>
    <tbody>
    <?php
    //iclude file koneksi ke database
    include('../config.php');

    //query ke database dg SELECT table siswa diurutkan berdasarkan NIS paling besar
    //    $query = mysql_query("SELECT * FROM m_barang ORDER BY nm_brg ASC ") or die(mysql_error());
    $query = mysqli_query($conn, "SELECT * FROM m_barang ORDER BY nm_brg ASC ");
    //cek, apakakah hasil query di atas mendapatkan hasil atau tidak (data kosong atau tidak)
    if(mysqli_num_rows($query) == 0){	//ini artinya jika data hasil query di atas kosong

        //jika data kosong, maka akan menampilkan row kosong
        echo '<tr><td colspan="6">Tidak ada data!</td></tr>';

    }else{	//else ini artinya jika data hasil query ada (data diu database tidak kosong)

        //jika data tidak kosong, maka akan melakukan perulangan while
        $no = 1;	//membuat variabel $no untuk membuat nomor urut
        while($data = mysqli_fetch_array($query)){	//perulangan while dg membuat variabel $data yang akan mengambil data di database

            //menampilkan row dengan data di database
            echo '<tr>';
            echo '<td>'.$data['id'].'</td>';	//menampilkan nomor urut
            echo '<td>'.$data['nm_brg'].'</td>';	//menampilkan data nis dari database
            echo '<td>'.$data['nm_principle'].'</td>';	//menampilkan data nama lengkap dari database
            echo '<td><center>'.$data['pack_ori'].'</center></td>';	//menampilkan data kelas dari database
//            echo '<td>'.$data['siswa_jurusan'].'</td>';	//menampilkan data jurusan dari database
//            echo '<td><a href="edit.php?id='.$data['id'].'">Edit</a> / <a href="hapus.php?id='.$data['id'].'" onclick="return confirm(\'Yakin?\')">Hapus</a></td>';	//menampilkan link edit dan hapus dimana tiap link terdapat GET id -> ?id=siswa_id
            echo '</tr>';

            $no = $no +1;	//menambah jumlah nomor urut setiap row

        }

    }
    ?>
    </tbody>
</table>