<div class="container-fluid">
	<h1 class="mt-4">Input Sample </h1>
    <ol class="breadcrumb mb-4">
    	<li class="breadcrumb-item ">Input Sample</li>
        <li class="breadcrumb-item active">Input Sample</li>
    </ol>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Input Sample Anda disini...
                </div>
                <div class="card-body">
                <form action="" method="post">
                    <div class="mb-3 row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                        <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="email@example.com">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                        <input type="password" class="form-control" id="inputPassword">
                        </div>
                    </div>

                    <Button class="btn btn-success">Add Item</Button>
                </form> 
                </div>
                </div>
            </div>
    </div>
</div>
<script>
    jQuery(window).load(function(){
        jQuery('#exampleModal').modal('show').on('hide.bs.modal', function(e){
        e.preventDefault();
        });
    });
</script>